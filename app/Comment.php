<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table="comments";
    protected $with='user';
    public function post(){
       return $this->belongsTo(Post::class);
    }
    public function getIsEditableAttribute($post_id){
        return  $liked = Comment::where('user_id',auth('api')->user()->id)->where('post_id',$post_id)->count() > 0 ? true:false;

    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
