<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArticleRequest as StoreRequest;
use App\Http\Requests\ArticleRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\UploadImage;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings('article', 'articles');
        $this->crud->setHeading("Новини");
        $this->crud->setTitle("Новини");


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
       // $this->crud->setFromDb();
        //Columns
        $this->crud->addColumn([
            'name'=>'title',
            'label'=>"Заглавие",
            'type'=>"text"
        ]);
        $this->crud->addColumn([
            'name'=>'img',
            'label'=>"Снимка",
            'type'=>"image"
        ]);
        $this->crud->addColumn([
            'name'=>'content',
            'label'=>"Съдържание",
            'type'=>"text"
        ]);
        $this->crud->addColumn([
            'name'=>'newsletter',
            'label'=>"За списанието",
            'type'=>"text"
        ]);
        //Fields
        $this->crud->addField([   // Browse
            'name' => 'title',
            'label' => 'Заглавие',
            'type' => 'text'
        ]);
        $this->crud->addField([   // Browse
            'name' => 'img',
            'label' => 'Снимка',
            'type' => 'browse'
        ]);
        $this->crud->addField(   // Browse
            [   // TinyMCE
                'name' => 'content',
                'label' => 'Съдърждание',
                'type' => 'tinymce'
            ]);
        $this->crud->addField(   // Browse
            [   // TinyMCE
                'name' => 'newsletter',
                    'label' => "Newsletter",
                    'type' => 'select2_from_array',
                    'options' => ["1" => "Да", "0" => "не"],
                    'allows_null' => false,
                    'default' => 'false',
                    // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;

            ]);
        $this->crud->addField(
            [  // Select2
                'label' => "Category",
                'type' => 'select2',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // foreign key model

                // optional
                'default' => 2, // set the default value of the select2
                'options'   => (function ($query) {
                    return $query->where('newsletter', 1)->get();
                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ]
        );

        // add asterisk for fields that are required in ArticleRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getArticles(){
        $articles = Article::latest()->paginate(6);
        return response()->json([
           "articles"=>$articles
        ]);
    }
    public function getArticle($id){
        $article = Article::find($id);
        return response()->json([
           "article"=>$article
        ]);
    }
    public function getNewsletter(){
        $articles = Article::where("newsletter",1)->latest()->get();
        return response()->json([
            "articles"=>$articles
        ]);
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
