<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BranchRequest as StoreRequest;
use App\Http\Requests\BranchRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Psy\Util\Json;

/**
 * Class BranchCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BranchCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Branch');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/branch');
        $this->crud->setEntityNameStrings('branch', 'branches');
        $this->crud->setHeading("Клонове и Партньори");
        $this->crud->setTitle("Клонове и Партньори");

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        //Columns
        $this->crud->addColumns([
            [
            // 1-n relationship
            'label' => "Име", // Table column heading
            'type' => "text",
            'name' => 'name', // the column that contains the ID of that connected entity;
        ],
       [
            // 1-n relationship
            'label' => "Снимка", // Table column heading
            'type' => "image",
            'name' => 'img', // the column that contains the ID of that connected entity;
        ],
        [
            // 1-n relationship
            'label' => "Локация", // Table column heading
            'type' => "text",
            'name' => 'location', // the column that contains the ID of that connected entity;
        ],

        [
            // 1-n relationship
            'label' => "Телефонни Номер", // Table column heading
            'type' => "select",
            'name' => 'branch_id', // the column that contains the ID of that connected entity;
            'entity' => 'phones', // the method that defines the relationship in your Model
            'attribute' => "phone", // foreign key attribute that is shown to user
            'model' => "App\Models\Branch", // foreign key model
         ]
        ]);


        //Fields
        $this->crud->addFields([
             [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Име',
                'tab'=>'Генерални'
            ],
            [
                'name' => 'img',
                'type' => 'browse',
                'label' => 'Снимка',
                'tab'=>'Генерални'
            ],
            [
                'name' => 'location',
                'type' => 'text',
                'tab'=>'Локация',
                'label' => 'Локация',
            ],

           [
               'name'=>'lat',
               'type'=>'number',
               'step'=>'any',
               'attributes' => ["step" => "any"],
               'label'=>'Координати',
               'tab'=>'Локация'
           ],
            [
               'name'=>'lng',
               'type'=>'number',
                'attributes' => ["step" => "any"],
                'label'=>'Координати',
                'tab'=>'Локация'
           ],
            [
                'name' => 'location',
                'type' => 'text',

                'label' => 'Адрес',
                'tab'=>'Локация'

            ],


            [ // Table
                'name' => 'options',
                'label' => 'Телефонни номера',
                'type' => 'table',
                'entity_singular' => 'phone', // used on the "Add X" button
                'columns' => [
                    'phone' => 'Телефонен номер',

                ],
                'max' => 5, // maximum rows allowed in the table
                'min' => 1, // minimum rows allowed in the table,
                'tab'=>'Генерални'
            ],

            [ // Table
                'name' => 'is_branch',
                'label' => 'Клон или партньор създавате',
                'type' => 'radio',
                'options'     => [ // the key will be stored in the db, the value will be shown as label;
                    1 => "Клон",
                    0 => "Партньор"
                ],
                'tab'=>'Генерални'
            ] ,
            [ // Table
                'name' => 'website',
                'label' => 'Website',
                'type' => 'text',

                'tab'=>'Генерални'
            ]
            ]);


        // add asterisk for fields that are required in BranchRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here

        $redirect_location = parent::storeCrud($request);
        foreach (json_decode($request->options) as $phone){
            $this->crud->entry->store_phone_number($this->crud->entry->id,$phone);

        }
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    //Custom functions
    public function getBranches(){
        $branches = Branch::where("is_branch",1)->with("phones")->get();
        return response()->json([
            "branches"=>$branches
        ]);
    }
    public function getPartners(){
        $partners = Branch::where("is_branch",0)->with("phones")->latest()->get();
        return response()->json([
            "partners"=>$partners
        ]);
    }
}
