<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest as StoreRequest;
use App\Http\Requests\CategoryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');
        $this->crud->setHeading("Категории");

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        //Columns
        $this->crud->addColumn([
            'name'=>'title',
            'label'=>'Заглавие',
            'type'=>'text'
        ]);
        $this->crud->addField([
            "name"=>"title",
            "label"=>"Заглавие",
            "type"=>"text"
        ]);
        $this->crud->addField(   // Browse
            [   // TinyMCE
                'name' => 'newsletter',
                'label' => "Newsletter",
                'type' => 'select_from_array',
                'options' => ["1" => "Да", "0" => "Не"],
                'allows_null' => false,
                'default' => '0',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;

            ]);

        // add asterisk for fields that are required in CategoryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getNewsletterCategories(){
        $categories = Category::where("newsletter",1)->get();
        return response()->json([
            "pages"=>$categories
        ]);
    }
    public function GetNewsletter($id){
        $articles = Category::find($id)->articles()->get();
        return response()->json([
            "articles"=>$articles
        ]);
    }
}
