<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EventRequest as StoreRequest;
use App\Http\Requests\EventRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\Models\Event;

/**
 * Class EventCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EventCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Event');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/event');
        $this->crud->setEntityNameStrings('event', 'events');
        $this->crud->setTitle("Събития");
        $this->crud->setHeading("Събития");

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
//Columns
        $this->crud->addColumn([
            "name"=>"name",
            "label"=>"Име",
            "type"=>"text"
        ]);
        $this->crud->addColumn([
            "name"=>"image",
            "label"=>"Снимка",
            "type"=>"image"
        ]);
        $this->crud->addColumn([
            "name"=>"description",
            "label"=>"Описание",
            "type"=>"text"
        ]);

        //Fields
        $this->crud->addField([
            "type"=>"text",
            "label"=>"Име",
            "name"=>"name"
        ]);
        $this->crud->addField([
            "type"=>"browse",
            "label"=>"Снимка",
            "name"=>"image"
        ]);
        $this->crud->addField([
            "type"=>"tinymce",
            "label"=>"Описание",
            "name"=>"description"
        ]);

        $this->crud->addField([
            // DateTime
            'name' => 'event_date',
            'type' => 'date_picker',
            'label' => 'Дата',
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'dd-mm-yyyy',
                'language' => 'bg'
            ],

        ]);
        $this->crud->addField([
            // DateTime
            'name' => 'event_time',
            'type' => 'time',
            'label' => 'Час',
            // optional:


        ]);


        // add asterisk for fields that are required in EventRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function getEvents(){
        $events = Event::latest()->get();
        return response()->json([
            "events"=>$events
        ]);
}
    public function getEvent($id){
        $event = Event::find($id);
        return response()->json([
            "event"=>$event
        ]);
    }
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
