<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PageRequest as StoreRequest;
use App\Http\Requests\PageRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\Models\Page;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('page', 'pages');
        //$this->crud->addButtonFromModelFunction('line', 'active', 'status'); // add a button whose HTML is returned by a method in the CRUD model
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        //Functions

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        //Columns
        $this->crud->addColumn([
            "label"=>"Заглавие",
            "name"=>"title",
            "type"=>"text"
        ]);
        $this->crud->addColumn([
            "label"=>"Съдържание",
            "name"=>"content",
            "type"=>"text"
        ]);
//        $this->crud->addColumn([
//
//                'name'=> 'active',
//                'label' => 'Статус',
//                'type' => 'radio',
//                'options'     => [
//                    0 => "Неактивна",
//                    1 => "Активна"
//                ]
//        ]);
        $this->crud->addColumn([
            'type' => 'active_inactive_button',
            'name' => 'active',
            'label' => "Статус",
           "options"=>[
               0=>"Неактивна",
               1=>"Активна",

           ]
    ]);

        //Fields
        $this->crud->addField(
            [
                'label' => 'Страница',
                'name' => 'page_type',
                'type' => 'toggle',
                'inline' => true,
                'options' => [
                    0 => 'Стандартна',
                    1 => 'Подстраница'
                ],
                'hide_when' => [
                    0 => ['page_id'],
                ],
                'default'=>0,
                'both'

            ]);
        $this->crud->addField([
            'label'=>"Избери страница",
            "name"=>"page_id",
            "type"=>"select",


            'entity' => 'page', // the method that defines the relationship in your Model
            'attribute' => 'title', // foreign key attribute that is shown to user
            'model' => "App\Models\Page"

        ]);
        $this->crud->addField([
            'label'=>'Заглавие',
            'name'=>'title',
            'type'=>'text'
        ]);
        $this->crud->addField([
            'label'=>'Съдържание',
            'name'=>'content',
            'type'=>'tinymce'
        ]);

        // add asterisk for fields that are required in PageRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function pages(){
        $pages = Page::with("pages")->get();
        return response()->json([
            "pages"=>$pages
        ]);
    }
    public function page($id){
        $page = Page::find($id);
        return response()->json([
            "page"=>$page
        ]);
    }
}
