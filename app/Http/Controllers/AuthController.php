<?php

namespace App\Http\Controllers;

use App\Events\ChangeStatus;
use App\Http\Requests\PasswordChangeRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\ImageUpload;

class AuthController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function register(RegisterRequest $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        return $this->login($request);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }


        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        $this->change_status(1);
        $user = auth('api')->user();
        $user['is_editable'] = $user->getIsEditableAttribute();
        return response()->json([
            "user"=> $user
        ]);

    }
    public function getUser($id){
        $user = User::where("id",$id)->first();

        $user['is_editable'] = $user->getIsEditableAttribute();

        return response()->json([
            "user"=>$user
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->change_status(0);
        auth('api')->logout();


        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }
    public function users(){
<<<<<<< HEAD

        $users = User::where("id","!=",auth('api')->user()->id)->with('threads')->latest()->get();
        $auth_user = auth('api')->user();
        foreach ($users as $user){
           foreach ($user->threads as $thread){
              if($thread->participantsUserIds($auth_user->id)!=null){
                    $user->setAttribute("thread_id",$thread->id);
               }
               //;

           }
        }
=======
        $users = User::where("id","!=",auth('api')->user()->id)->latest()->get();
        foreach ($users as $user){
            foreach ($user->threads as $thread){
                if(count($thread->participantsUserIds(auth('api')->user()->id))>0 ){
                    $user->setAttribute("thread_id",$thread->id);
                }
                //;
            }
        }
        $users = $users->map(function(User $myuser){

            $myuser['unread'] = $myuser->getIsUnreadAttribute($myuser->thread_id,$myuser->id);

            return $myuser;
        });
>>>>>>> feature/dev
        return response()->json([
            "users"=>$users
        ]);

    }
    public function ChangePassword(PasswordChangeRequest $request){
            $user = User::find(auth('api')->user()->id);
            if($user->password == bcrypt($request->password)){
                if(bcrypt($request->new_password) != $user->password ){
                    $request["password"] = bcrypt($request->new_password);
                    return $this->patch($request);
                }else{
                    return response()->json([
                        "message"=>"Новата парола не може да бъде същата като старата!"
                    ],500);
                }
            }else{
                return response()->json([
                    "message"=>"Паролите не съвпадат!"
                ],500);
            }
            }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'is_approved'=>auth('api')->user()->is_approved,
            'is_paid'=>auth('api')->user()->is_paid,


        ]);
    }
    public function patch(Request $request){

        $user =Auth()->user();
        if($request->has("img")){

            $url = ImageUpload::upload($request->img,$request->img_name);

                $request["img"] = $url;

        }
        $user =Auth()->user();
        if($request->has("bg_image")){

            $url = ImageUpload::upload($request->bgurl,$request->img_name);

                $request["bg_image"] = $url;

        }

        $user->update($request->only(["email","name","password","img","img_name","bg_image"]));
        $user->save();
        return response()->json([
           "status"=>"success"
        ]);
    }
    public function change_status($status){
        $user = Auth()->user();
        $user->active = $status;
        $user->save();
        event(new ChangeStatus($user->id,$status));
    }
}
