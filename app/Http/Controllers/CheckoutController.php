<?php

namespace App\Http\Controllers;


use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    //
    public function subscribe(Request $request){

        $token = $request->stripe_token['id'];
        $user = auth('api')->user();
        if(!$user->is_subscribed) {
            $plan = "plan_FfvNExd0UhLSfx";
            $user->is_bookkeeper = $request->is_bookkeeper;
            $user->organization = $request->organization;
            $user->city = $request->city;
            $user->profession = $request->profession;
            $user->is_subscribed = true;
            $user->save();
            $user->newSubscription("main", $plan)->create($token);
            return response()->json([
                "status"=>"subscribed"
            ]);
        }else{
            return response()->json(["
            status" =>"Вече си абониран"],500);
        }


    }
    public function unsubscribe(){
        $user = auth('api')->user();
        $user->subscription('main')->cancelNow();
        $user->is_subscribed = false;
        $user->save();
        return response()->json([
           "status"=>"unsubscribed"
        ]);
    }

}
