<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    //
    public function store(Request $request){
        $comment = new Comment();
        $comment->post_id = $request->post_id;
        $comment->user_id = auth('api')->user()->id;
        $comment->description = $request->description;
        $comment->save();
        $comment->load('user');

        return response()->json([
            'status'=>'ok',
            'comment'=>$comment
        ]);
    }
    public function delete($post_id,$id){
        Comment::find($id)->delete();
        return response()->json([
           'status'=>'ok',

        ],200);
    }
    public function patch($post_id,$id,Request $request){
        $comment = Comment::findorFail($id);
        $comment->description = $request->description;
        $comment->save();
        return response()->json([
           'status'=>'ok',

        ],200);
    }
}
