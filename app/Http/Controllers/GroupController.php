<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    //
    public function store(Request $request){
        $group = new Group();
        $group->user_id = auth()->user()->id;
        $group->name = $request->name;
        $group->save();
    }
}
