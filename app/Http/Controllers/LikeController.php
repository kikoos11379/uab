<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    //
    public function store(Request $request){
        $like = Like::where("user_id",auth('api')->user()->id)->where("post_id",$request->id)->first();
        if($like != null){
            $like->delete();
        }else {
            $like = new Like();
            $like->post_id = $request->id;
            $like->user_id = auth('api')->user()->id;
            $like->save();
        }

    }
    public function delete(Request $request){
        Like::where('post_id',$request->id)->where('user_id',auth('api')->user()->id)->delete();
        return response()->json([
            'status'=>'ok'
        ],200);
    }
}
