<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use Illuminate\Http\Request;
use App\User;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
<<<<<<< HEAD
    public function get($id){
        $thread = "";
       if($this->isThreadFound($id)){
           $thread = Thread::find($id)->with('messages')->first();

        }else{
           $thread = Thread::create([
               'subject' => "New message",
           ]);
           return;
       }
        $userId = auth()->user()->id;
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);


        return response()->json([
           "thread"=>$thread
=======
    public function get($id,$friend_id){
        $thread = "";
        if($this->isThreadFound($id)){
            $thread = Thread::where("id",$id)->with('messages','messages.user')->first();

        }else{
            $thread = Thread::create([
                'subject' => "New message",
            ]);
            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => auth('api')->user()->id,
                'last_read' => new Carbon(),
            ]);
            $thread->addParticipant($friend_id);
        }
        $userId = auth('api')->user()->id;

        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();


        $thread->markAsRead($userId);
        $thread->load(['messages','messages.user']);


        return response()->json([
            "thread"=>$thread
        ]);
    }
    private function isThreadFound($id){

            $thread = Thread::where("id",$id)->count();
            if($thread > 0){
                return true;
            }


            return false;

    }
    public function store(Request $request){


            $this->updateThread($request);

    }

    private function updateThread(Request $request){

        $message = Message::create([
            'thread_id' => $request->thread_id,
            'user_id' =>auth('api')->user()->id,
            'body' => $request->message,
>>>>>>> feature/dev
        ]);
        if(!Participant::where("thread_id",$request->thread_id)->count() > 1){
            Participant::create([
                'thread_id' => $request->thread_id,
                'user_id' => $request->id,
                'last_read' => new Carbon(),
            ]);
        }

        $participant = Participant::where('user_id',$request->id)->first();
       $participant->last_read = new Carbon();
        $participant->save();
    $message->load('user');
        $user = User::find($request->id);
        event(new NewMessage(auth('api')->user()->id,$message,$user));
    }
<<<<<<< HEAD
    private function isThreadFound($id){
        try {
            $thread = Thread::findOrFail($id);
            return true;
        } catch (ModelNotFoundException $e) {
           return false;
        }
    }
    public function store(Request $request){

        if($this->isThreadFound($request->thread_id)){

            $this->updateThread($request);
        }else{
           $this->createThread($request);
        }
    }
    private function createThread(Request $request){
        $thread = Thread::create([
            'subject' => "Message",
        ]);
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => auth('api')->user()->id,
            'body' => $request->message,
        ]);
        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => auth('api')->user()->id,
            'last_read' => new Carbon,
        ]);
        $thread->addParticipant($request->id);
        return response()->json([
           'status'=>'OK'
        ],200);
    }

    private function updateThread(Request $request){

        Message::create([
            'thread_id' => $request->thread_id,
            'user_id' =>auth('api')->user()->id,
            'body' => $request->message,
        ]);
        $participant = Participant::where('user_id',$request->id)->first();

        $participant->last_read = new Carbon;
        $participant->save();
    }
=======
    public function lastMessages(){
        $threads =auth('api')->user()->threads;
        foreach ($threads as $thread){
            $thread->load('users');
            $thread->load('messages');
            foreach ($thread->users as $key=>$user){
                if($user->id == auth('api')->user()->id) {
                        unset($thread->users[$key]);
                }else {
                    $user->setAttribute("thread_id", $thread->id);
                    $thread->users[0] = $user;
                }
            }
        }

        return response()->json([
           'threads'=>$threads
        ]);
    }
    public function seen(Request $request){
        Message::where('thread_id',$request->thread_id)->where('user_id',$request->friend_id)->latest()->limit(1)->update(['status'=>'1']);
        return response()->json([
           'status'=>'ok'
        ]);
    }
>>>>>>> feature/dev
}
