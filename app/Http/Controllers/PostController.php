<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Image;
use App\ImageUpload as post_image;

class PostController extends Controller
{
    //
    public function get($fetch,$user_id=null){
        $posts = null;
        if($user_id == null) {
            $posts = Post::latest()->with(["user", "images", "likes", "comments" => function ($query) {
                $query->latest();
            }, "comments.user"])->skip($fetch)->take(5)->get();

        }else{
            $posts = Post::latest()->where('user_id',$user_id)->with(["user", "images", "likes", "comments" => function ($query) {
                $query->latest();
            }, "comments.user"])->skip($fetch)->take(5)->get();

        }
        $posts = $posts->map(function(Post $mypost){

            $mypost['is_liked'] = $mypost->getIsLikedAttribute();
            $mypost['is_editable'] = $mypost->getIsEditableAttribute();

            return $mypost;
        });

        return response()->json([
            "posts" => $posts
        ]);
    }
    public function store(Request $request){
        $post = new Post();
        $post->user_id = auth()->user()->id;
        $post->content = $request->post_content;

        $post->save();
        if($request->images){
            foreach ($request->images as $img){
                $image = post_image::upload($img["url"],$img["name"]);
                $post->images()->create([
                    "url"=>$image,
                    "name"=>$img["name"]
                ]);
            }
        }
        return response()->json([
            "status"=>"uploaded"
        ]);
    }
    public function patch(Request $request){
        $post = Post::find($request->id);
        $post->content = $request->post_content;
        $post->save();
        if($request->images){
            foreach ($request->images as $img){
<<<<<<< HEAD
                if(substr($img["url"],0,4)=='data') {
                    $url  = 'users/posts/'. auth()->user()->id . '/';
                    post_image::upload($img["url"], $img["name"],$url);
                    $post->images()->create([
                        "url" => $url. $img['name'],
=======
                if($this->startsWith($img['url'],"data")) {
                    $image = post_image::upload($img["url"], $img["name"]);
                    $post->images()->create([
                        "url" => $image,
>>>>>>> feature/dev
                        "name" => $img["name"]
                    ]);
                }
            }
        }
        return response()->json([
            "status"=>"uploaded"
        ]);

    }
    public function delete($id){
        $post = Post::find($id);
        $post->delete();
    }
    public function removeImage($post_id,$id){
<<<<<<< HEAD
        $post = post_image::destroy($id);
=======
        $post = Post::find($post_id);
       $image =  $post->images()->find($id);
       $image->delete();
>>>>>>> feature/dev
        return response()->json([
           'status'=>'OK'
        ],200);
    }
    private function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }
}
