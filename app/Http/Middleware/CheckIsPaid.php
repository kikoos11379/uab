<?php

namespace App\Http\Middleware;

use Closure;

class CheckIsPaid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->is_paid){
            return redirect("/#/social/subscription");
        }
        return $next($request);
    }
}
