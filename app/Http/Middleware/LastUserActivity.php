<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Cache;

class LastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('api')->check()) {
            $expiresAt = Carbon::now()->addMinutes(1);
            Cache::put('user-is-online-' . auth('api')->user()->id, true, $expiresAt);
        }
        return $next($request);
    }
}
