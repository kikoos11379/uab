<?php

namespace App;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    //
    protected $fillable  = ["branch_id","phone"];
    public function branch(){
        return $this->belongsTo(Branch::class);
    }
}
