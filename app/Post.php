<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $fillable = ['post_content'];
    public function likes(){
       return $this->hasMany(Like::class);
    }
    public function comments(){
       return $this->hasMany(Comment::class);
    }
   public function  getIsLikedAttribute(){
       return  $liked = Like::where('user_id',auth('api')->user()->id)->where('post_id',$this->id)->count() > 0 ? true:false;
   }
   public function  getIsEditableAttribute(){
       return  $this->user_id == auth('api')->user()->id ? true:false;

   }
    public function images(){
       return $this->hasMany(ImageUpload::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function saveImage($image,$name){
        $images = new ImageUpload();
        $images->post_id = $this->id;
        $images->url = $image;
        $images->name = $name;
        $images->save();
    }
}
