<?php

namespace App;

use Cmgmyr\Messenger\Models\Participant;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Cmgmyr\Messenger\Traits\Messagable;

use Cmgmyr\Messenger\Models\Thread;

use Cmgmyr\Messenger\Models\Message;



class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use Billable;
    use Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password','img','bg_image','is_approved','is_subscribed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function last_messages(){

        $threads = Thread::forUserWithNewMessages($this->id())->latest('updated_at')->get();
        return $threads;

    }



}
