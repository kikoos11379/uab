/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import router from './router/router.js'
import mix from './mixins/mixin.js'
import Vue from 'vue'
import App from './components/App'
import axios from 'axios'
import store from './store/store.js'
import vuetify from './vuetify/vuetify.js'
import VueChatScroll from 'vue-chat-scroll'
import infiniteScroll from 'vue-infinite-scroll'
import VueScrollTo from 'vue-scrollto'
import * as VueGoogleMaps from 'vue2-google-maps'


Vue.use(VueScrollTo)


Vue.use(VueChatScroll);

Vue.use(VueGoogleMaps, {
        load: {
            key: 'AIzaSyCMJZYUHrFH7EB2cGKrK91jZv6RyDDShnw',
            libraries: 'places',
        }
    }
)
Vue.use(infiniteScroll)
axios.defaults.baseURL = 'http://uab.bg/api/'
require('./bootstrap');

window.Vue = require('vue');
Vue.prototype.$EventBus = new Vue();



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/App.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


Vue.mixin(mix);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    vuetify,
    template: '<App/>',
    router,
    store,
    components: { App }
});
