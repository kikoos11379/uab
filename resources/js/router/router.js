import Vue from 'vue'
import Router from 'vue-router'
import News from '../components/Home/News/News'
import SocialApp from '../components/Social1/App'
import Messages from '../components/Social1/Messages/Messages'
import Home from '../components/Home/Home'
import Checkout from '../components/Social/Subscription/Checkout'
import SocialHome from '../components/Social1/Home.vue'
import Profile from '../components/Social1/Profile/Profile.vue'
import Subscription from "../components/Social/Subscription/Subscription";
import Contacts from "../components/Home/Contacts/Contacts";
import Events from "../components/Home/Events/Events";
import Cookies from "../components/Home/Cookies/Cookies";
import Event from "../components/Home/Events/Event";
import SocialEvents from "../components/Social1/Events/Events";
import store from "../store/store";
import Page from "../components/Home/Page/Page";
import Branches from "../components/Home/Branches/Branches";
import NewsInfo from "../components/Home/News/NewsInfo";
import Partners from "../components/Home/Partners/Partners";
import Login from "../components/Home/Auth/Login";
import Register from "../components/Home/Auth/Register";




Vue.use(Router)

let router = new Router({
    mode:'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component:Home,
        },
        {
            path: '/news',
            name: 'News',
            component:News
        },
        {
            path: '/cookies',
            name: 'Cookies',
            component:Cookies
        },
        {
            path: '/about',
            name: 'Page',
            component:Page,
            props:{default: true,id:1}
        },
        {
            path: '/usefull',
            name: 'Page',
            component:Page,
            props:{default:true,id:2}
        },
        {
            path: '/contacts',
            name: 'Contacts',
            component:Contacts,
            //props:{id:2}
        },
        {
            path: '/news/article/:id',
            name: 'NewsInfo',
            component:NewsInfo,
            props:true
        },
        {
            path: '/branches',
            name: 'Page',
            component:Branches,


        },
        {
            path: '/events',
            name: 'Events',
            component:Events,


        },
        {
            path: '/login',
            name: 'Login',
            component:Login,


        },

        {
            path: '/events/event/:id',
            name: 'EventInfo',
            component:Event,
           props:true

        },
        {
            path: '/payment',
            name: 'Payment',
            component:Checkout,

        },
        {
            path: '/partners',
            name: 'Partners',
            component:Partners,


        },
        {
            path: '/register',
            name: 'Register',
            component:Register
        },
        //Social
        {
            path: '/social/',
            name: 'SocialApp',

            component:SocialApp,
            props:true,
            meta:{
                requiresApprove:true
            },
            children:[
                {
                    path: '',
                    components: {
                        social: SocialHome //note that 'b' is the name of child router view
                    }
                },
                {
                    path: 'user/:id',

                    components: {
                        social: Profile,

                        //note that 'b' is the name of child router view
                    },
                    props:true,
                } ,
                {
                    path: 'profile',
                    components: {
                        social: Profile //note that 'b' is the name of child router view
                    }
                } ,

                {
                    path: 'messages',
                    components: {
                        social: Messages //note that 'b' is the name of child router view
                    }
                },
                {
                    path: 'events',
                    components: {
                        social: SocialEvents //note that 'b' is the name of child router view
                    }
                },
                {
                    path: 'subscription',
                    components: {
                        social: Subscription //note that 'b' is the name of child router view
                    }
                }
            ],
        },
        //Newsletter
        {
            path:'/newsletter',
            name:'Home',
            component:Home,
            props:{newsletter:true},
            meta:{
                requiresApprove:true
            }
        }

    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresApprove )) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!localStorage.is_approved) {
            next({
                path: '/',

            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
    if (to.matched.some(record => record.meta.requiresPayment)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!localStorage.is_paid) {
            next({
                path: '/social/subscription',
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})
export default router;
