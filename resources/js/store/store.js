import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        user: {},
        is_approved:false,
        is_paid:false
    },
    mutations: {
        setUser (state,user) {
            this.state.user = user;
        },
        setApproved (state) {
           state.is_approved = true;
        },
        setPaid (state) {
           state.is_paid = true;
        },
        removeUser(state){
            this.state.user = null;
        }
    },
    getters:{
        getUser:state => {
            return state.user;
        }
    },
    actions:{
        getUser:context=>{
            this.get('auth/user').then(function (resp) {

                context.commit('setUser',resp);
            }).catch((err)=>{


            });
        }
    }
});
export default store;
