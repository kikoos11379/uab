<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
{{--@if(backpack_user()->hasRole("Администратор"))--}}
<li><a href="{{ backpack_url('dashboard') }}"><i class="fas fa-tachometer-alt"></i> <span>Начало</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fas   fa-files-o"></i> <span>Файлов мениджър</span></a></li>
<li><a href='{{ backpack_url('article') }}'><i class="fas fa-newspaper"></i> <span> Новини</span></a></li>
<li><a href='{{ backpack_url('event') }}'><i class="fas fa-calendar-week"> </i> <span> Събития</span></a></li>
<li><a href='{{ backpack_url('page') }}'><i class='fas fa-pager'></i> <span>Страници</span></a></li>
<li><a href='{{ backpack_url('branch') }}'><i class="fas fa-code-branch"></i> <span>Клонове и партьори</span></a></li>
<li><a href='{{ backpack_url('category') }}'><i class='fa fa-tag'></i> <span>Категории</span></a></li>
<!-- Users, Roles Permissions -->

<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Потребители</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Потребители</span></a></li>
        <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Роли</span></a></li>
        <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Разрешения</span></a></li>
    </ul>
</li>

</li>
{{--@endif--}}
@if(backpack_user('web')->hasRole("Редактор"))
    <li><a href='{{ backpack_url('article') }}'><i class="fas fa-newspaper"></i> <span> Новини</span></a></li>
    @endif

