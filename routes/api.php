<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => ['jwt.auth','cors','activity'],
    'prefix' => 'auth'

], function ($router) {

//User

    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user', 'AuthController@user');
    Route::get('users/user/{id}', 'AuthController@getUser');
    Route::get('users', 'AuthController@users');
    Route::get("last/user/messages","MessageController@last_messages");
    Route::patch("profile/update","AuthController@patch");
    //Messages
<<<<<<< HEAD
    Route::get('messages/{id}', 'MessageController@get');
    Route::post('messages/add', 'MessageController@store');
    //Route::post("messages/seen","MessageController@seen");
=======
    Route::get('messages/threads/thread/{id}/friend/{friend_id}', 'MessageController@get');
    Route::post('messages/thread/{thread_id}/add', 'MessageController@store');
    Route::get('user/messages','MessageController@lastMessages');
    Route::post('user/threads/seen','MessageController@seen');
>>>>>>> feature/dev
    //Group
    Route::post('groups/create', 'GroupController@store');
    Route::get('groups', 'GroupController@get');
    //Posts
    Route::get("posts/fetch/{fetch}","PostController@get");
    Route::get("posts/fetch/{fetch}/user/{user_id}","PostController@get");
    Route::post("posts/create","PostController@store");
    Route::delete("/posts/{id}/delete","PostController@delete");


    Route::post("posts/post/{id}/like","LikeController@store");
    Route::delete("posts/post/{id}/dislike","LikeController@delete");

    Route::post("posts/post/{id}/comment","CommentController@store");
    Route::delete("posts/post/{post_id}/comment/{id}/delete","CommentController@delete");
    Route::patch("posts/post/{post_id}/comment/{id}/patch","CommentController@patch");

    Route::patch("post/{id}/patch","PostController@patch");
    Route::delete("post/{post_id}/image/{id}/delete","PostController@removeImage");
    //Payment
    Route::post("subscribe", "CheckoutController@subscribe");
    Route::post("unsubscribe","CheckoutController@unsubscribe");


});
Route::group([

    'middleware' => ['cors',],

], function ($router) {
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/register', 'AuthController@register');
//News
    Route::get("/articles", "Admin\ArticleCrudController@getArticles");
    Route::get("/article/{id}", "Admin\ArticleCrudController@getArticle");
//Events
    Route::get("/events/", "Admin\EventCrudController@getEvents");
    Route::get("/events/event/{id}", "Admin\EventCrudController@getEvent");

    Route::get("/page/{id}", "Admin\PageCrudController@page");
//Branches and partners
    Route::get("/branches/", "Admin\BranchCrudController@getBranches");
    Route::get("/partners/", "Admin\BranchCrudController@getPartners");
//Newsletter
    Route::get("/newsletter/", "Admin\ArticleCrudController@getNewsletter");
    Route::get("/newsletter/category/{id}", "Admin\CategoryCrudController@getNewsletter");
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
